package com.app.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value = {"classpath:internal-prop.properties", "classpath:application.properties"}, ignoreResourceNotFound = true, name = "myServerConfigs")
@SpringBootApplication
@ComponentScan("com.example.demo")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
